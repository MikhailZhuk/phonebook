package com.getjavajob.web06.zhukm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "employee_tbl")
@NamedQueries({
        @NamedQuery(name = "Employee.filter.names", query = "SELECT e.firstName,e.middleName,e.lastName,e.id FROM Employee e " +
                "WHERE LOWER(CONCAT(e.firstName,e.middleName,e.lastName) )" +
                "LIKE LOWER(CONCAT('%',:filter,'%'))"),
        @NamedQuery(name = "Employee.byDepartment", query = "SELECT e FROM Employee e " +
                "WHERE e.department.id=:departmentId")
})
@JsonIgnoreProperties("photo")
public class Employee extends BaseEntity implements Serializable {
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "last_name")
    private String lastName;
    private String email;
    private String icq;
    @Lob
    @Transient
    private Blob photo;
    private String skype;
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    @Column(name = "extra_info")
    private String extraInfo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "boss_id")
    private Employee boss;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "home_address_id")
    private HomeAddress homeAddress;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "work_address_id")
    private WorkAddress workAddress;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "employee_to_work_phone", joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "work_phone_id"))
    private List<WorkPhone> workPhones = new ArrayList<>();
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "employee_id")
    private List<PersonalPhone> personalPhones = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public WorkAddress getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(WorkAddress workAddress) {
        this.workAddress = workAddress;
    }

    public HomeAddress getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(HomeAddress homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public Employee getBoss() {
        return boss;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<WorkPhone> getWorkPhones() {
        return workPhones;
    }

    public void setWorkPhones(List<WorkPhone> workPhones) {
        this.workPhones = workPhones;
    }

    public void addWorkPhone(WorkPhone phone) {
        workPhones.add(phone);
    }

    public void clearWorkPhones() {
        workPhones = null;
    }

    public Blob getPhoto() {
        return photo;
    }

    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

    public List<PersonalPhone> getPersonalPhones() {
        return personalPhones;
    }

    public void addPersonalPhone(PersonalPhone phone) {
        personalPhones.add(phone);
    }

    public void setPersonalPhones(List<PersonalPhone> personalPhones) {
        this.personalPhones = personalPhones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        if (firstName != null ? !firstName.equals(employee.firstName) : employee.firstName != null) return false;
        if (middleName != null ? !middleName.equals(employee.middleName) : employee.middleName != null) return false;
        if (lastName != null ? !lastName.equals(employee.lastName) : employee.lastName != null) return false;
        if (email != null ? !email.equals(employee.email) : employee.email != null) return false;
        if (icq != null ? !icq.equals(employee.icq) : employee.icq != null) return false;
        if (skype != null ? !skype.equals(employee.skype) : employee.skype != null) return false;
        if (birthdate != null ? !birthdate.equals(employee.birthdate) : employee.birthdate != null) return false;
        if (extraInfo != null ? !extraInfo.equals(employee.extraInfo) : employee.extraInfo != null) return false;
        if (department != null ? !department.equals(employee.department) : employee.department != null) return false;
        if (boss != null ? !boss.equals(employee.boss) : employee.boss != null) return false;
        if (homeAddress != null ? !homeAddress.equals(employee.homeAddress) : employee.homeAddress != null)
            return false;
        if (workAddress != null ? !workAddress.equals(employee.workAddress) : employee.workAddress != null)
            return false;
        if (workPhones != null ? !workPhones.equals(employee.workPhones) : employee.workPhones != null) return false;
        return !(personalPhones != null ? !personalPhones.equals(employee.personalPhones) : employee.personalPhones != null);
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (icq != null ? icq.hashCode() : 0);
        result = 31 * result + (skype != null ? skype.hashCode() : 0);
        result = 31 * result + (birthdate != null ? birthdate.hashCode() : 0);
        result = 31 * result + (extraInfo != null ? extraInfo.hashCode() : 0);
        result = 31 * result + (department != null ? department.hashCode() : 0);
        result = 31 * result + (boss != null ? boss.hashCode() : 0);
        result = 31 * result + (homeAddress != null ? homeAddress.hashCode() : 0);
        result = 31 * result + (workAddress != null ? workAddress.hashCode() : 0);
        result = 31 * result + (workPhones != null ? workPhones.hashCode() : 0);
        result = 31 * result + (personalPhones != null ? personalPhones.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return firstName + " " + middleName + " " + lastName;
    }
}