package com.getjavajob.web06.zhukm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "department_tbl")
public class Department extends BaseEntity implements Serializable {
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "head_id")
    private Employee head;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "department_id")
    private List<DepartmentPhone> phones = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getHead() {
        return head;
    }

    public void setHead(Employee head) {
        this.head = head;
    }

    public void addPhone(DepartmentPhone phone) {
        phones.add(phone);
    }

    public List<DepartmentPhone> getPhones() {
        return phones;
    }

    public void setPhones(List<DepartmentPhone> phones) {
        this.phones = phones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        if (!name.equals(that.name)) return false;
        if (head != null ? !head.equals(that.head) : that.head != null) return false;
        return !(phones != null ? !phones.equals(that.phones) : that.phones != null);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (head != null ? head.hashCode() : 0);
        result = 31 * result + (phones != null ? phones.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }
}