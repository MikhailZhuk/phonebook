package com.getjavajob.web06.zhukm;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "personal_phone_tbl")
public class PersonalPhone extends Phone implements Serializable {
}