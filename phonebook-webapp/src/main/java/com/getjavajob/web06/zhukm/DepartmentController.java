package com.getjavajob.web06.zhukm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class DepartmentController {

    private static final Logger logger = LoggerFactory.getLogger(DepartmentController.class);
    private static final String CREATED_MSG = "Сотрудник создан";
    private static final String UPDATED_MSG = "Сотрудник отредактирован";
    private static final String DELETED_MSG = "Сотрудник удалён";
    @Autowired
    private ServiceLayer service;

    @RequestMapping(value = "/department", method = RequestMethod.GET)
    public ModelAndView show(@RequestParam("id") int id) {
        logger.debug("show(id=%s)", id);
        ModelAndView mav = new ModelAndView("showDepartment");
        Department department = service.getDepartment(id);
        mav.addObject("department", department);
        mav.addObject("theseEmployees", service.getByDepartment(department));
        return mav;
    }

    @RequestMapping(value = "/createDepartment")
    public ModelAndView showCreate() {
        ModelAndView mav = new ModelAndView("createDepartment");
        mav.addObject("employees", service.getEmployees());
        return mav;
    }

    @RequestMapping(value = "doCreateDepartment", method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute() Department department, BindingResult errors) {
        logger.debug("create(), department=%s ", department);
        if (errors.hasErrors()) {
            List<FieldError> lerr = errors.getFieldErrors();
            for (FieldError err : lerr) {
                logger.error("createDepartment() " + err);
            }
        }
        department = service.createDepartment(department);
        ModelAndView mad = new ModelAndView("redirect:/department?id=" + department.getId());
        mad.addObject("notification", CREATED_MSG);
        return mad;
    }

    @RequestMapping(value = "/editDepartment", method = RequestMethod.GET)
    public ModelAndView showEdit(@RequestParam("id") int id) {
        logger.debug("showEdit(id=%s)", id);
        ModelAndView mav = new ModelAndView("editDepartment");
        mav.addObject("department", service.getDepartment(id));
        mav.addObject("employees", service.getEmployees());
        return mav;
    }

    @RequestMapping(value = "/doUpdateDepartment", method = RequestMethod.POST)
    public ModelAndView update(@ModelAttribute() Department department, BindingResult errors) {
        logger.debug("update(), department=%s ", department);
        if (errors.hasErrors()) {
            List<FieldError> lerr = errors.getFieldErrors();
            for (FieldError err : lerr) {
                logger.error("update() " + err);
            }
        }
        department = service.updateDepartment(department);
        ModelAndView mav = new ModelAndView("redirect:/department?id=" + department.getId());
        mav.addObject("notification", UPDATED_MSG);
        return mav;
    }

    @RequestMapping(value = "/departments")
    public ModelAndView showDepartments() {
        ModelAndView mav = new ModelAndView("departments");
        mav.addObject("departments", service.getAllDepartments());
        return mav;
    }

    @RequestMapping(value = "/deleteDepartment", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam("id") int id) {
        logger.debug("delete(), id:%s ", id);
        service.deleteDepartment(service.getDepartment(id));
        ModelAndView mav = new ModelAndView("departments");
        mav.addObject("notification", DELETED_MSG);
        mav.addObject("departments", service.getAllDepartments());
        return mav;
    }
}