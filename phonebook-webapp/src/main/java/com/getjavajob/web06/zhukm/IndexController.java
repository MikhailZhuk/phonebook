package com.getjavajob.web06.zhukm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/error")
    public void throwException() {
        Integer.parseInt("Error to example");
    }
}