package com.getjavajob.web06.zhukm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
public class EmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private static final int UPIC_HEIGHT_PX = 200;
    private static final int UPIC_WIDTH_PX = 200;
    private static final String CREATED_MSG = "Сотрудник создан";
    private static final String UPDATED_MSG = "Сотрудник отредактирован";
    private static final String DELETED_MSG = "Сотрудник удалён";
    @Autowired
    private ServiceLayer service;

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public ModelAndView show(@RequestParam("id") int id) {
        logger.debug("show(id=%s)", id);
        ModelAndView mav = new ModelAndView("showEmployee");
        mav.addObject("employee", service.getEmployee(id));
        return mav;
    }

    @RequestMapping(value = "/createEmployee")
    public ModelAndView showCreate() {
        ModelAndView mav = new ModelAndView("createEmployee");
        mav.addObject("departments", service.getAllDepartments());
        mav.addObject("employees", service.getEmployees());
        return mav;
    }

    @RequestMapping(value = "doCreateEmployee", method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute() Employee employee, @RequestParam(value = "birthdate-string", required = false) String birthdate, @RequestParam("file-photo") MultipartFile filePhoto, BindingResult errors) {
        logger.debug("create(), employee=%s ", employee);
        if (errors.hasErrors()) {
            List<FieldError> lerr = errors.getFieldErrors();
            for (FieldError err : lerr) {
                logger.error("createEmployee() " + err);
            }
        }
        if (!filePhoto.isEmpty()) {
            try {
                employee.setPhoto(resizeAndConvert(filePhoto));
            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        }
        if (birthdate != null && !birthdate.isEmpty()) {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            try {
                employee.setBirthdate(format.parse(birthdate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        employee = service.createEmployee(employee);
        ModelAndView mav = new ModelAndView("redirect:/employee?id=" + employee.getId());
        mav.addObject("notification", CREATED_MSG);
        return mav;
    }

    @RequestMapping(value = "/editEmployee", method = RequestMethod.GET)
    public ModelAndView showEdit(@RequestParam("id") int id) {
        logger.debug("showEdit(id=%s)", id);
        ModelAndView mav = new ModelAndView("editEmployee");
        mav.addObject("employee", service.getEmployee(id));
        mav.addObject("departments", service.getAllDepartments());
        mav.addObject("employees", service.getEmployees());
        return mav;
    }

    @RequestMapping(value = "/doUpdateEmployee", method = RequestMethod.POST)
    public ModelAndView update(@ModelAttribute() Employee employee, @RequestParam(value = "birthdate-string", required = false) String birthdate, BindingResult errors, @RequestParam("file-photo") MultipartFile filePhoto) {
        logger.debug("update(), employee=%s ", employee);
        if (errors.hasErrors()) {
            List<FieldError> lerr = errors.getFieldErrors();
            for (FieldError err : lerr) {
                logger.error("update() " + err);
            }
        }
        if (!filePhoto.isEmpty()) {
            try {
                employee.setPhoto(resizeAndConvert(filePhoto));
            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        } else {
            if (service.getEmployee(employee.getId()).getPhoto() != null) {
                employee.setPhoto(service.getEmployee(employee.getId()).getPhoto());
            }
        }
        if (birthdate != null && !birthdate.isEmpty()) {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            try {
                employee.setBirthdate(format.parse(birthdate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        service.updateEmployee(employee);
        ModelAndView mav = new ModelAndView("redirect:/employee?id=" + employee.getId());
        mav.addObject("notification", UPDATED_MSG);
        return mav;
    }

    private Blob resizeAndConvert(MultipartFile filePhoto) throws IOException, SQLException {
        String extension = (filePhoto.getOriginalFilename().split("\\."))[1];
        byte[] originalBytes = filePhoto.getBytes();
        InputStream in = new ByteArrayInputStream(originalBytes);
        BufferedImage originalImage = ImageIO.read(in);
        in.close();
        Blob blob;
        if (originalImage.getWidth() != 200 && originalImage.getHeight() != 200) {
            BufferedImage resizedImage = new BufferedImage(UPIC_WIDTH_PX, UPIC_HEIGHT_PX, originalImage.getType());
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(originalImage, 0, 0, UPIC_WIDTH_PX, UPIC_HEIGHT_PX, null);
            g.dispose();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(resizedImage, extension, baos);
            baos.flush();
            blob = new SerialBlob(baos.toByteArray());
            baos.close();
        } else {
            blob = new SerialBlob(originalBytes);
        }
        return blob;
    }

    @RequestMapping(value = "/ajaxFilter")
    @ResponseBody
    public List<Employee> searchEmployee(@RequestParam("filter") String filter) {
        List<Employee> employees = service.searchEmployeeNames(filter);
        logger.debug("searchEmployee(), ajax result:%s ", employees);
        return employees;
    }

    @RequestMapping(value = "/employees")
    public ModelAndView showEmployees(@RequestParam(value = "filter", required = false) String filter) {
        logger.debug("showEmployees(), filter:%s ", filter);
        ModelAndView mav = new ModelAndView("employees");
        if (filter != null) {
            filter = filter.trim();
        }
        if (filter != null && !filter.isEmpty()) {
            mav.addObject("employees", service.searchEmployee(filter));
            mav.addObject("notification", filter);
        } else {
            mav.addObject("employees", service.getEmployees());
        }
        return mav;
    }

    @RequestMapping(value = "/employees/userpic", method = RequestMethod.GET)
    public void displayBlob(@RequestParam("id") int id, HttpServletResponse response) {
        logger.debug("displayBlob(), id:%s ", id);
        Blob photo = service.getEmployee(id).getPhoto();
        try (ServletOutputStream stream = response.getOutputStream()) {
            byte[] bytes = photo.getBytes(1, (int) photo.length());
            stream.write(bytes);
            stream.flush();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/deleteEmployee", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam("id") int id) {
        logger.debug("delete(), id:%s ", id);
        service.deleteEmployee(service.getEmployee(id));
        ModelAndView mav = new ModelAndView("employees");
        mav.addObject("notification", DELETED_MSG);
        mav.addObject("employees", service.getEmployees());
        return mav;
    }
}