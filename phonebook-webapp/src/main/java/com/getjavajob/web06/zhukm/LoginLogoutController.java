package com.getjavajob.web06.zhukm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

@Controller
public class LoginLogoutController {

    private static final Logger logger = LoggerFactory.getLogger(LoginLogoutController.class);
    private HashMap<String, String> validLoginPassword = new HashMap<>();
    @Autowired
    private ServletContext servletContext;

    @PostConstruct
    private void init() {
        Properties props = new Properties();
        try {
            props.load(servletContext.getResourceAsStream("/resources/login.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String login = props.getProperty("login");
        String password = props.getProperty("password");
        validLoginPassword.put(login, password);
    }

    @RequestMapping(value = "/login")
    public String showLogin() {
        return "login";
    }

    @RequestMapping(value = "/ajaxLogin", method = RequestMethod.POST)
    @ResponseBody
    public String ajaxLogin(@RequestParam("login") String login, @RequestParam("password") String password, @RequestParam("remember") String remember,
                            HttpServletResponse response, HttpSession session) throws ServletException, IOException {
        String validation;
        if (password.equals(validLoginPassword.get(login))) {
            session.setAttribute("login", login);
            if (Boolean.parseBoolean(remember)) {
                Cookie cookie = new Cookie("remember_me", login);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
            validation = "yes";
        } else {
            logger.info("Invalid sign in attempt, login=%s, password=%s", login, password);
            validation = "no";
        }
        return validation;
    }

    @RequestMapping(value = "/logOut")
    public String logOut(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }
        session.invalidate();
        return "index";
    }
}