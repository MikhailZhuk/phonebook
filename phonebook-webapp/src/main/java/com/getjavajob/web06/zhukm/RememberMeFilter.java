package com.getjavajob.web06.zhukm;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class RememberMeFilter implements Filter {
    private List<String> validLogin = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Properties props = new Properties();
        try {
            props.load(filterConfig.getServletContext().getResourceAsStream("/resources/login.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String login = props.getProperty("login");
        validLogin.add(login);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        if (session.getAttribute("login") == null) {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (validLogin.contains(cookie.getValue())) {
                        session.setAttribute("login", cookie.getValue());
                    }
                }
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}