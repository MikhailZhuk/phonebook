var context;
function setContext(o) {
    context = o;
}
$("#ajax-search").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: context.val + "/ajaxFilter",
            data: {
                filter: request.term
            },
            success: function (data) {
                response($.map(data, function (employee, i) {
                    return {
                        value: employee.id,
                        label: employee.firstName + ' ' + employee.middleName + ' ' + employee.lastName
                    }
                }));
            },

        })
    },
    minLength: 1,
    select: function (event, ui) {
        window.location = context.val + '/employee?id=' + ui.item.value;
    }
});