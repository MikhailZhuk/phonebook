$("#add-personal-phone").click(function () {
    $("#personal-phones").append('<div class="input-group"><span class="input-group-btn"><button class="btn btn-danger btn-sm delete" type="button"><i class="material-icons">delete</i></button></span><input type="text" class="form-control personal-phone is-numeric" data-trigger="manual" data-toggle="popover" data-content="Только цифры"> </div>');
});

$("#add-work-phone").click(function () {
    $("#work-phones").append('<div class="input-group"><span class="input-group-btn"><button class="btn btn-danger btn-sm delete" type="button"><i class="material-icons">delete</i></button></span><input type="text" class="form-control work-phone is-numeric" data-trigger="manual" data-toggle="popover" data-content="Только цифры"></div>');
});

$("#add-department-phone").click(function () {
    $("#department-phones").append('<div class="input-group"><span class="input-group-btn"><button class="btn btn-danger btn-sm delete" type="button"><i class="material-icons">delete</i></button></span><input type="text" class="form-control department-phone is-numeric" data-trigger="manual" data-toggle="popover" data-content="Только цифры"></div>');
});

$("body").on("click", ".delete", function () {
    $(this).parent("span").parent("div").remove();
});

function renamePersonalPhones() {
    var с = 0;
    $(".personal-phone").each(function () {
        $(this).attr('name', 'personalPhones[' + с + '].number');
        с = с + 1;
    })
}

function renameWorkPhones() {
    var с = 0;
    $(".work-phone").each(function () {
        $(this).attr('name', 'workPhones[' + с + '].number');
        с = с + 1;
    })
}

function renameDepartmentPhones() {
    var с = 0;
    $(".department-phone").each(function () {
        $(this).attr('name', 'phones[' + с + '].number');
        с = с + 1;
    })
}