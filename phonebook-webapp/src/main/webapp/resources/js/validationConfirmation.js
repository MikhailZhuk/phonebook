//DYNAMIC VALIDATION

function validateNumeric(p) {
    if (!$.isNumeric(p.val())) {
        p.popover('show');
        return false;
    } else {
        p.popover('hide');
        return true;
    }
}

function validateNull(o) {
    var value = o.val().trim();
    if (value == null || value == "") {
        o.popover('show');
        return false;
    } else {
        o.popover('hide');
        return true;
    }
}

var errorText;
function setErrorText(o) {
    errorText = o;
}

$("#save-btn").click(function (e) {
    e.preventDefault();
    var validationOk = true;

    $(".is-numeric").each(function () {
        if (!$.isNumeric($(this).val())) {
            validationOk = false;
        }
    });
    $(".not-null").each(function () {
        var value = $(this).val().trim();
        if (value == null || value == "") {
            validationOk = false;
        }
    });
    if (validationOk == true) {
        $('#modal-confirm').modal('show');
    } else {
        $('.top-right').notify({
            message: {text: errorText.val},
            fadeOut: {enabled: true, delay: 3000},
            closable: false,
            type: 'danger'
        }).show();
    }
});

$("#modal-save-employee-btn").click(function (e) {
    renamePersonalPhones();
    renameWorkPhones();
    renameDepartmentPhones();
    $("#employee-form").submit();
    e.preventDefault();
});

$("#modal-save-department-btn").click(function (e) {
    renameDepartmentPhones();
    $("#department-form").submit();
    e.preventDefault();
});

$("#delete-employee").click(function (e) {
    e.preventDefault();
    $('#modal-confirm').modal('show');
    $("#modal-delete-employee-btn").click(function () {
        window.location.replace($('#delete-employee').attr('href'));
    });
});

$("#delete-department").click(function (e) {
    e.preventDefault();
    $('#modal-confirm').modal('show');
    $("#modal-delete-department-btn").click(function () {
        window.location.replace($('#delete-department').attr('href'));
    });
});