function opacityDown(input) {
    $(input).stop().fadeTo("fast", 0.2);
}
function opacityUp(input) {
    $(input).stop().fadeTo("slow", 1);
}