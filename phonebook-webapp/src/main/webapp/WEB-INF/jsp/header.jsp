<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<a href="${context}/">
    <div class=""><h3>Телефонная книга</h3>
        <small class="text-muted">Java, Spring MVC, Hibernate, MySql, JSP, Tomcat, Javascript, JQuery</small>
    </div>
</a>
<div class="navbar navbar-inverse margin-top-bottom-20">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse navbar-inverse-collapse">
            <ul class="nav navbar-nav">
                <li><a href="${context}/employees">Сотрудники</a></li>
                <li><a href="${context}/departments">Департаменты</a></li>
                <!-- DROPDOWN -->
                <c:if test="${not empty login}">
                    <li class="dropdown">
                        <a href="http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html"
                           data-target="#" class="dropdown-toggle" data-toggle="dropdown">Создать
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="${context}/createEmployee">Сотрудника</a></li>
                            <li><a href="${context}/createDepartment">Департамент</a></li>
                        </ul>
                    </li>
                </c:if>
            </ul>
            <!-- SEARCH-->
            <form class="navbar-form navbar-left form-ajax-search" action="${context}/employees" method="post">
                <div class="form-group" style="display:inline;">
                    <input type="text" id="ajax-search" class="form-control" name="filter" placeholder="Поиск">
                    <span class="material-input"></span>
                </div>
            </form>
            <!-- LOGIN -->
            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                    <c:when test="${empty login}">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="${context}/login">Войти</a></li>
                        </ul>
                    </c:when>
                    <c:otherwise>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Разное
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="${context}/error">Бросить исключение</a></li>
                                <li><a href="${context}/404">404</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons md-18">person</i></span>${login}<span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="${context}/logOut">Выйти</a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </div>
</div>