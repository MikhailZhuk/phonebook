<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Редактирование: ${employee}</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="col-md-8 col-md-offset-2 margin-top-bottom-20">
        <div class="well page bs-component">
            <form class="form-horizontal" id="employee-form" method="POST" action="${context}/doUpdateEmployee"
                  enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        <small>Редактирование</small>
                    </legend>
                    <input type="hidden" name="id" value="${employee.id}">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <c:choose>
                                <c:when test="${empty employee.photo}">
                                    <img class="img-circle img-responsive img-center" id="userpic"
                                         src="${context}/resources/images/200x200.png"
                                         style="width: 200px; height: 200px">
                                </c:when>
                                <c:otherwise>
                                    <img class="img-circle img-responsive img-center" id="userpic"
                                         src="${context}/employees/userpic?id=${employee.id}"
                                         style="width: 200px; height: 200px">
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="col-md-2">
                            <label for="file-upload" class="custom-file-upload">
                                <button class="btn btn-info btn-fab">
                                    <i class="material-icons">file_upload</i></button>
                            </label>
                            <input id="file-upload" type="file" name="file-photo" onchange="showUploadedUpic(this)"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Имя</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control not-null" name="firstName"
                                   value="${employee.firstName}"
                                   data-toggle="popover" data-trigger="manual"
                                   data-content="Обязательное поле, не может быть пустым">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Отчество</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control not-null" name="middleName"
                                   value="${employee.middleName}"
                                   data-toggle="popover" data-trigger="manual"
                                   data-content="Обязательное поле, не может быть пустым">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Фамилия</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control not-null" name="lastName"
                                   value="${employee.lastName}"
                                   data-toggle="popover" data-trigger="manual"
                                   data-content="Обязательное поле, не может быть пустым">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">День рождения</label>
                        <div class="col-md-10">
                            <fmt:formatDate pattern="dd.MM.yyyy" value="${employee.birthdate}" var="birthdate"/>
                            <input type="text"
                                   value="<fmt:formatDate pattern="dd.MM.yyyy" value="${employee.birthdate}"/>"
                                   class="form-control" name="birthdate-string" id="datepicker"
                                   placeholder="День рождения">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="s1">Выберите департамент в котором работает сотрудник</label>
                            <select id="s1" class="form-control" name="department.id">
                                <c:forEach items="${departments}" var="department">
                                    <c:choose>
                                        <c:when test="${[employee.department.id]==[department.id]}">
                                            <option value="${department.id}" selected>${department.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${department.id}">${department.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="s2">Выберите начальника</label>
                            <select id="s2" class="form-control" name="boss.id">
                                <c:forEach items="${employees}" var="boss">
                                    <c:choose>
                                        <c:when test="${[employee.id]==[boss.id]}">
                                        </c:when>
                                        <c:when test="${[employee.boss.id]==[boss.id]}">
                                            <option value="${boss.id}" selected>${boss.toString()}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${boss.id}">${boss.toString()}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <h3>Контактные данные</h3>
                    <div class="form-group">
                        <label class="col-md-2 control-label">E-Mail</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="email" value="${employee.email}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Skype</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="skype" value="${employee.skype}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">ICQ</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="icq" value="${employee.icq}">
                        </div>
                    </div>
                    <h4>Личные телефоны</h4>
                    <div class="form-group">
                        <div class="col-md-10" id="personal-phones">
                            <c:forEach items="${employee.personalPhones}" var="phone">
                                <div class="input-group">
                                <span class="input-group-btn">
                                <button class="btn btn-danger btn-sm delete" type="button">
                                    <i class="material-icons">delete</i></button></span>
                                    <input type="text" class="form-control personal-phone is-numeric"
                                           data-trigger="manual"
                                           data-toggle="popover" data-content="Только цифры" value="${phone.number}">
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-4">
                        <input class="btn btn-raised btn-success btn-sm" id="add-personal-phone" value="Добавить поле"/>
                    </div>
                    <div class="row"></div>
                    <h4>Рабочие номера телефонов</h4>
                    <div class="form-group">
                        <div class="col-md-10" id="work-phones">
                            <c:forEach items="${employee.workPhones}" var="phone">
                                <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-danger btn-sm delete" type="button">
                                <i class="material-icons">delete</i></button></span>
                                    <input type="text" class="form-control work-phone is-numeric"
                                           value="${phone.number}" data-trigger="manual" data-toggle="popover"
                                           data-content="Только цифры">
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-4">
                        <input class="btn btn-raised btn-success btn-sm" id="add-work-phone" value="Добавить поле"/>
                    </div>
                    <div class="row"></div>
                    <h3>Домашний адрес</h3>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Страна</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.country"
                                   value="${employee.homeAddress.country}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Город</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.city"
                                   value="${employee.homeAddress.city}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Улица</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.street"
                                   value="${employee.homeAddress.street}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дом</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.house"
                                   value="${employee.homeAddress.house}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Квартира</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.room"
                                   value="${employee.homeAddress.room}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Почтовый индекс</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.zip"
                                   value="${employee.homeAddress.zip}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дополнительная информация</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.extra"
                                   value="${employee.homeAddress.extra}">
                        </div>
                    </div>
                    <h3>Адрес работы</h3>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Страна</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.country"
                                   value="${employee.workAddress.country}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Город</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.city"
                                   value="${employee.workAddress.city}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Улица</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.street"
                                   value="${employee.workAddress.street}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дом</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.house"
                                   value="${employee.workAddress.house}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Квартира</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.room"
                                   value="${employee.workAddress.room}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Почтовый индекс</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.zip"
                                   value="${employee.workAddress.zip}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дополнительная информация</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.extra"
                                   value="${employee.workAddress.extra}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-1">
                            <input type="text" class="form-control" name="extraInfo" value="${employee.extraInfo}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <input type="submit" class="btn btn-raised btn-info btn-lg" value="Сохранить"
                                   id="save-btn"/>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Сохранить изменения?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-raised btn-success" id="modal-save-employee-btn">Да</button>
            </div>
        </div>
    </div>
</div>
<div class='notifications top-right'></div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        var context = {val: '${context}'};
        setContext(context);

        $('[data-toggle="popover"]').popover();

        $('#alert-box').hide();

        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "dd.mm.yy"
            });
        });

        $(document).on('focusout', '.is-numeric', function () {
            validateNumeric($(this));
        });

        $(document).on('focusout', '.not-null', function () {
            validateNull($(this));
        });

        var errorText = {val: "Не все поля заполнены правильно. Обязательно заполните ФИО. В телефонах только цифры."};
        setErrorText(errorText)
    });

    function showUploadedUpic(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#userpic').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
</body>
</html>