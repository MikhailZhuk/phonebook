<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Создание сотрудника</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="col-md-8 col-md-offset-2 margin-top-bottom-20">
        <div class="well page bs-component">
            <form class="form-horizontal" id="employee-form" method="POST" action="${context}/doCreateEmployee"
                  enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        <small>Создать сотрудника</small>
                    </legend>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <img class="img-circle img-responsive img-center" id="userpic"
                                 src="${context}/resources/images/200x200.png" style="width: 200px; height: 200px">
                        </div>
                        <div class="col-md-2">
                            <label for="file-upload" class="custom-file-upload">
                                <button class="btn btn-info btn-fab">
                                    <i class="material-icons">file_upload</i></button>
                            </label>
                            <input id="file-upload" type="file" name="file-photo" onchange="showUploadedUpic(this)"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Имя</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control not-null" name="firstName" placeholder="Имя"
                                   data-toggle="popover" data-trigger="manual"
                                   data-content="Обязательное поле, не может быть пустым">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Отчество</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control not-null" name="middleName" placeholder="Отчество"
                                   data-toggle="popover" data-trigger="manual"
                                   data-content="Обязательное поле, не может быть пустым">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Фамилия</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control not-null" name="lastName" placeholder="Фамилия"
                                   data-toggle="popover" data-trigger="manual"
                                   data-content="Обязательное поле, не может быть пустым">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">День рождения</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="birthdate-string" id="datepicker"
                                   placeholder="День рождения">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="s1">Выберите департамент</label>
                            <select id="s1" class="form-control" name="department.id">
                                <c:forEach items="${departments}" var="department">
                                    <option value="${department.id}">${department.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="s2">Выберите начальника</label>
                            <select id="s2" class="form-control" name="boss.id">
                                <c:forEach items="${employees}" var="employee">
                                    <option value="${employee.id}">${employee.toString()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <h3>Контактные данные</h3>
                    <div class="form-group">
                        <label class="col-md-2 control-label">E-Mail</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Skype</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="skype">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">ICQ</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="icq">
                        </div>
                    </div>
                    <h4>Личные телефоны</h4>
                    <div class="form-group">
                        <div class="col-md-10" id="personal-phones">
                            <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-danger btn-sm delete" type="button">
                                <i class="material-icons">delete</i></button></span>
                                <input type="text" class="form-control personal-phone is-numeric" data-trigger="manual"
                                       data-toggle="popover" data-content="Только цифры">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-4">
                        <input class="btn btn-raised btn-success btn-sm" id="add-personal-phone" value="Добавить поле"/>
                    </div>
                    <div class="row"></div>
                    <h4>Рабочие номера телефонов</h4>
                    <div class="form-group">
                        <div class="col-md-10" id="work-phones">
                            <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-danger btn-sm delete" type="button">
                                <i class="material-icons">delete</i></button></span>
                                <input type="text" class="form-control work-phone is-numeric" data-trigger="manual"
                                       data-toggle="popover" data-content="Только цифры">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-4">
                        <input class="btn btn-raised btn-success btn-sm" id="add-work-phone" value="Добавить поле"/>
                    </div>
                    <div class="row"></div>
                    <h3>Домашний адрес</h3>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Страна</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.country">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Город</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.city">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Улица</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.street">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дом</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.house">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Квартира</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.room">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Почтовый индекс</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.zip">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дополнительная информация</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="homeAddress.extra">
                        </div>
                    </div>
                    <h3>Адрес работы</h3>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Страна</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.country">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Город</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.city">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Улица</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.street">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дом</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.house">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Квартира</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.room">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Почтовый индекс</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.zip">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Дополнительная информация</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="workAddress.extra">
                        </div>
                    </div>
                    <h3>Дополнительная информация</h3>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-1">
                            <textarea class="form-control" rows="3" id="textArea" name="extraInfo"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <input type="submit" class="btn btn-raised btn-info btn-lg" value="Сохранить"
                                   id="save-btn"/>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Создать нового сотрудника?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-raised btn-success" id="modal-save-employee-btn">Да</button>
            </div>
        </div>
    </div>
</div>
<div class='notifications top-right'></div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        var context = {val: '${context}'};
        setContext(context);
        $('[data-toggle="popover"]').popover();
        $('#alert-box').hide();

        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "dd.mm.yy"
            });
        });

        $(document).on('focusout', '.is-numeric', function () {
            validateNumeric($(this));
        });

        $(document).on('focusout', '.not-null', function () {
            validateNull($(this));
        });

        var errorText = {val: "Не все поля заполнены правильно. Обязательно заполните ФИО. В телефонах только цифры."};
        setErrorText(errorText)
    });

    function showUploadedUpic(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#userpic').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
</body>
</html>