<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Департаменты</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="table-responsive">
        <c:forEach var="department" items="${departments}">
            <a href="${context}/department?id=${department.id}">
                <div class="col-md-2 col-sm-6 text-center" style="height: 220px" onmouseover="opacityDown(this)"
                     style="padding: 15px" onmouseout="opacityUp(this)">
                    <h1 class="text-warning" style="font-size: 6em">
                        <strong>${fn:substring([department.name], 1, 2)}</strong></h1>
                    <h5 class="text-danger">${department.name}</h5>
                </div>
            </a>
        </c:forEach>
    </div>
</div>
<div class='notifications top-right'></div>
<jsp:include page="scripts.jsp"/>
<script>
    <c:if test="${not empty notification}">
    $('.top-right').notify({
        message: {text: '${notification}'},
        fadeOut: {enabled: true, delay: 1500},
        closable: false,
        type: 'info'
    }).show();
    </c:if>

    var context = {val: '${context}'};
    setContext(context);
</script>
</body>
</html>