<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>${department.name}</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="col-md-8 col-md-offset-2 margin-top-bottom-20">
        <div class="well page bs-component">
            <fieldset>
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="text-warning">${department.name}</h1></p>
                        <h3></h3>
                        <div class="col-md-6 col-md-offset-1 text-center">
                            <c:choose>
                                <c:when test="${empty department.head.photo}">
                                    <img class="img-circle img-responsive img-center"
                                         src="${context}/resources/images/200x200.png" alt="">
                                </c:when>
                                <c:otherwise>
                                    <img class="img-circle img-responsive img-center"
                                         src="${context}/employees/userpic?id=${department.head.id}" alt="">
                                </c:otherwise>
                            </c:choose>
                            <small>Начальник</small>
                            <br>
                            <a href="${context}/employee?id=${department.head.id}"
                               class="text-primary">${department.head}</a>
                        </div>
                    </div>
                    <div class="col-md-4 padding-top-bottom-20">
                        <div class="col-md-12">
                            <h4>Телефоны</h4>
                            <c:forEach items="${department.phones}" var="phone">
                                <p>${phone.number}</p>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-md-2 padding-top-bottom-20">
                        <c:if test="${not empty login}">
                            <p><a href="${context}/editDepartment?id=${department.id}"
                                  class="btn btn-info btn-sm btn-fab">
                                <i class="material-icons">mode_edit</i>
                            </a></p>
                            <p>
                                <button href="${context}/deleteDepartment?id=${department.id}" id="delete-department"
                                        class="btn btn-danger btn-sm btn-fab">
                                    <i class="material-icons">delete</i>
                                </button>
                            </p>
                        </c:if>
                    </div>
                </div>
                <div class="row margin-top-bottom-20 text-center">
                    <small>Сотрудники департамента</small>
                </div>
                <div class="table-responsive">
                    <c:forEach var="employee" items="${theseEmployees}">
                        <a href="${context}/employee?id=${employee.id}">
                            <div class="col-md-2 col-sm-6 text-center" onmouseover="opacityDown(this)"
                                 style="padding: 22px" onmouseout="opacityUp(this)">
                                <c:choose>
                                    <c:when test="${empty employee.photo}">
                                        <img class="img-circle img-responsive center-block"
                                             src="${context}/resources/images/200x200.png" alt="">
                                    </c:when>
                                    <c:otherwise>
                                        <img class="img-circle img-responsive center-block"
                                             src="${context}/employees/userpic?id=${employee.id}" alt="">
                                    </c:otherwise>
                                </c:choose>
                                <h6 class="text-danger">${employee.toString()}</h6>
                            </div>
                        </a>
                    </c:forEach>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class='notifications top-right'></div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Удалить департамент?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-raised btn-success" id="modal-delete-department-btn">Да</button>
            </div>
        </div>
    </div>
</div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        <c:if test="${not empty param.notification}">
        $('.top-right').notify({
            message: {text: '${param.notification}'},
            fadeOut: {enabled: true, delay: 1500},
            closable: false,
            type: 'info'
        }).show();
        </c:if>

        var context = {val: '${context}'};
        setContext(context);
    })
</script>
</body>
</html>