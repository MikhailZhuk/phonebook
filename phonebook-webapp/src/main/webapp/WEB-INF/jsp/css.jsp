<link href="${context}/resources/css/bootstrap.css" rel="stylesheet">
<link href="${context}/resources/css/jquery-ui.css" rel="stylesheet">
<link href="${context}/resources/css/bootstrap-notify.css" rel="stylesheet">
<link href="${context}/resources/css/fonts.css" rel="stylesheet">
<link href="${context}/resources/css/bootstrap-material-design.css" rel="stylesheet">
<link href="${context}/resources/css/ripples.css" rel="stylesheet">
<link href="${context}/resources/css/icons.css" rel="stylesheet">
<link href="${context}/resources/css/custom.css" rel="stylesheet">