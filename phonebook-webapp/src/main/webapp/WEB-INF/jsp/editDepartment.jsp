<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>${department}</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="col-md-8 col-md-offset-2 margin-top-bottom-20">
        <div class="well page bs-component">
            <form class="form-horizontal" id="department-form" method="POST" action="${context}/doUpdateDepartment">
                <fieldset>
                    <legend>
                        <small>Редактирование</small>
                    </legend>
                    <input type="hidden" name="id" value="${department.id}">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Название департамента</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control not-null" name="name" value="${department.name}"
                                   data-toggle="popover" data-trigger="manual"
                                   data-content="Обязательное поле, не может быть пустым"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="s2">Выберите начальника департамента</label>
                            <select id="s2" class="form-control" name="head.id">
                                <c:forEach items="${employees}" var="head">
                                    <c:choose>
                                        <c:when test="${[department.head.id]==[head.id]}">
                                            <option value="${head.id}" selected>${head}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${head.id}">${head}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <h4>Телефоны департамента</h4>
                    <div class="form-group">
                        <div class="col-md-10" id="department-phones">
                            <c:forEach items="${department.phones}" var="phone">
                                <div class="input-group">
                                <span class="input-group-btn">
                                <button class="btn btn-danger btn-sm delete" type="button">
                                    <i class="material-icons">delete</i></button></span>
                                    <input type="text" class="form-control department-phone is-numeric"
                                           data-trigger="manual" data-toggle="popover" data-content="Только цифры"
                                           value="${phone.number}">
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-4">
                        <input class="btn btn-raised btn-success btn-sm" id="add-department-phone"
                               value="Добавить поле"/>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <input type="submit" class="btn btn-raised btn-info btn-lg" value="Сохранить"
                                   id="save-btn"/>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Сохранить изменения?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-raised btn-success" id="modal-save-department-btn">Да</button>
            </div>
        </div>
    </div>
</div>
<div class='notifications top-right'></div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();

        $('#alert-box').hide();

        $(document).on('focusout', '.is-numeric', function () {
            validateNumeric($(this));
        });

        $(document).on('focusout', '.not-null', function () {
            validateNull($(this));
        });

        var context = {val: '${context}'};
        setContext(context);

        var errorText = {val: "Не все поля заполнены правильно. Обязательно заполните название департамента. В телефонах только цифры."};
        setErrorText(errorText)
    });
</script>
</body>
</html>