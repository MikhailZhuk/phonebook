<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Вход в систему</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="col-md-6 col-md-offset-3 margin-top-bottom-20">
        <div class="well page bs-component">
            <form class="form-horizontal" id="employee-form" method="POST" action="asfasf">
                <fieldset>
                    <legend>
                        <small>Введите логин и пароль</small>
                    </legend>
                    <input type="hidden" name="id" value="${employee.id}">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Имя</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="login" name="login">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Отчество</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <div class="togglebutton">
                            <label>
                                <input type="checkbox" id="remember" name="remember" value="false">Запомнить
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <input type="submit" class="btn btn-raised btn-info btn-lg" value="Сохранить"
                                   id="enter-login"/>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class='notifications top-right'></div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        <c:if test="${not empty notification}">
        $('.top-right').notify({
            message: {text: '${notification}'},
            fadeOut: {enabled: true, delay: 1500},
            closable: false,
            type: 'info'
        }).show();
        </c:if>

        $("#enter-login").click(function (e) {
            e.preventDefault();
            $.post("${context}/ajaxLogin", {
                        login: $("#login").val(),
                        password: $("#password").val(),
                        remember: $("#remember").is(':checked')
                    })
                    .done(function (notification) {
                        if (notification == 'yes') {
                            window.location.replace("${context}/");
                        } else {
                            $('.top-right').notify({
                                message: {text: "Неправильный логин или пароль"},
                                fadeOut: {enabled: true, delay: 2000},
                                closable: false,
                                type: 'danger'
                            }).show();
                        }
                    });
        });
    });

    var context = {val: '${context}'};
    setContext(context);
</script>
</body>
</html>