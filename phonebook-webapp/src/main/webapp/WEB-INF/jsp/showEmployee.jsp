<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>${employee.toString()}</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="col-md-8 col-md-offset-2 margin-top-bottom-20">
        <div class="well page bs-component">
            <fieldset>
                <div class="row">
                    <div class="col-md-2">
                        <h2 class="text-warning">${employee.toString()}</h2>
                        <p>
                            <small class="text-muted">Дата рождения:</small>
                        </p>
                        <h5><fmt:formatDate pattern="dd.MM.yyyy" value="${employee.birthdate}"/></h5>
                        <p>
                            <small class="text-muted">Департамент</small>
                        </p>
                        <a href="${context}/department?id=${employee.department.id}">
                            <h5>${employee.department.name}</h5>
                        </a>
                        <p>
                            <small class="text-muted">Начальник</small>
                        </p>
                        <a href="${context}/employee?id=${employee.boss.id}">
                            <h5>${employee.boss}</h5>
                        </a>
                    </div>
                    <div class="col-md-6 col-md-offset-2 text-center">
                        <c:choose>
                            <c:when test="${empty employee.photo}">
                                <img class="img-circle img-responsive img-center"
                                     src="${context}/resources/images/200x200.png">
                            </c:when>
                            <c:otherwise>
                                <img class="img-circle img-responsive img-center"
                                     src="${context}/employees/userpic?id=${employee.id}">
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-md-2">
                        <c:if test="${not empty login}">
                            <p><a href="${context}/editEmployee?id=${employee.id}" class="btn btn-info btn-sm btn-fab">
                                <i class="material-icons">mode_edit</i>
                            </a></p>
                            <p>
                                <button href="${context}/deleteEmployee?id=${employee.id}"
                                        class="btn btn-danger btn-sm btn-fab" id="delete-employee">
                                    <i class="material-icons">delete</i>
                                </button>
                            </p>
                        </c:if>
                    </div>
                </div>
                <div class="row margin-top-bottom-20">
                    <div class="col-md-3">
                        <p>
                            <small class="text-muted">E-mail</small>
                        </p>
                        <p>${employee.email}</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <small class="text-muted">Skype</small>
                        </p>
                        <p>${employee.skype}</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <small class="text-muted">ICQ</small>
                        </p>
                        <p>${employee.icq}</p>
                    </div>
                </div>
                <div class="row margin-top-bottom-20">
                    <div class="col-md-6">
                        <p>
                            <small class="text-muted">Домашний адрес</small>
                        </p>
                        <p>${employee.homeAddress}</p>
                    </div>
                    <div class="col-md-6">
                        <p>
                            <small class="text-muted">Рабочий адрес</small>
                        </p>
                        <p>${employee.workAddress}</p>
                    </div>
                </div>
                <div class="row margin-top-bottom-20">
                    <div class="col-md-6">
                        <p>
                            <small class="text-muted">Личные телефоны</small>
                        </p>
                        <c:forEach items="${employee.personalPhones}" var="phone">
                            <p>${phone.number}</p>
                        </c:forEach>
                    </div>
                    <div class="col-md-6">
                        <p>
                            <small class="text-muted">Рабочие телефоны</small>
                        </p>
                        <c:forEach items="${employee.workPhones}" var="phone">
                            <p>${phone.number}</p>
                        </c:forEach>
                    </div>
                </div>
                <div class="row margin-top-bottom-20">
                    <div class="col-md-12">
                        <p>
                            <small class="text-muted">Дополнительная информация</small>
                        </p>
                        <p>${employee.extraInfo}</p>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class='notifications top-right'></div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Удалить сотрудника?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-raised btn-success" id="modal-delete-employee-btn">Да</button>
            </div>
        </div>
    </div>
</div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        <c:if test="${not empty param.notification}">
        $('.top-right').notify({
            message: {text: '${param.notification}'},
            fadeOut: {enabled: true, delay: 1500},
            closable: false,
            type: 'info'
        }).show();
        </c:if>

        var context = {val: '${context}'};
        setContext(context);
    })
</script>
</body>
</html>