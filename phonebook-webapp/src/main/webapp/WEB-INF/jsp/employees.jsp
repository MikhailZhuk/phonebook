<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Сотрудники</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="table-responsive">
        <c:forEach var="employee" items="${employees}">
            <a href="${context}/employee?id=${employee.id}">
                <div class="col-md-2 col-sm-6 text-center" style="height: 250px" onmouseover="opacityDown(this)"
                     style="padding: 15px" onmouseout="opacityUp(this)">
                    <c:choose>
                        <c:when test="${empty employee.photo}">
                            <img class="img-circle img-responsive center-block"
                                 src="${context}/resources/images/200x200.png" alt="">
                        </c:when>
                        <c:otherwise>
                            <img class="img-circle img-responsive center-block"
                                 src="${context}/employees/userpic?id=${employee.id}" alt="">
                        </c:otherwise>
                    </c:choose>
                    <h5 class="text-danger">${employee.toString()}</h5>
                </div>
            </a>
        </c:forEach>
    </div>
</div>
<div class='notifications top-right'></div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        <c:if test="${not empty notification}">
        $('.top-right').notify({
            message: {text: '${notification}'},
            fadeOut: {enabled: true, delay: 1500},
            closable: false,
            type: 'info'
        }).show();
        </c:if>
        var context = {val: '${context}'};
        setContext(context);
    });
</script>
</body>
</html>