<script src="${context}/resources/js/jquery.js"></script>
<script src="${context}/resources/js/jquery-ui.js"></script>
<script src="${context}/resources/js/bootstrap.js"></script>
<script src="${context}/resources/js/bootstrap-notify.js"></script>
<script src="${context}/resources/js/ripples.js"></script>
<script src="${context}/resources/js/material.js"></script>
<script src="${context}/resources/js/phones.js"></script>
<script src="${context}/resources/js/validationConfirmation.js"></script>
<script src="${context}/resources/js/ajax.js"></script>
<script src="${context}/resources/js/effects.js"></script>
<script>
    $.material.init();
</script>