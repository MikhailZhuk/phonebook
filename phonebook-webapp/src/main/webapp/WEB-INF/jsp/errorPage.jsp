<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Ошибка</title>
    <jsp:include page="css.jsp"/>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <div class="col-md-8 col-md-offset-2 margin-top-bottom-20">
        <div class="well page bs-component">
            <fieldset>
                <legend>
                    <small>Произошла ошибка</small>
                    ${pageContext.exception.printStackTrace()}
                </legend>
                <c:if test="${not empty pageContext.errorData.statusCode}">
                    <div class="col-md-12 text-center margin-20">
                        <small class="text-muted">Код ошибки</small>
                        <h1 class="text-danger">${pageContext.errorData.statusCode}</h1>
                    </div>
                </c:if>

                <c:if test="${not empty pageContext.errorData.requestURI}">
                    <div class="col-md-12 text-center margin-20">
                        <small class="text-muted">URI</small>
                        <h3 class="text-danger">${pageContext.errorData.requestURI}</h3>
                    </div>
                </c:if>

                <c:if test="${not empty pageContext.exception}">
                    <div class="col-md-12 margin-20 text-center">
                        <small>Исключение</small>
                        <h4 class="text-danger">${pageContext.exception}</h4>
                    </div>
                </c:if>

                <c:if test="${not empty pageContext.exception.stackTrace}">
                    <div class="col-md-12">
                        <small>Stacktrace</small>

                        <c:forEach var="trace" items="${pageContext.exception.stackTrace}">
                            <p class="text-danger hyphen">${trace}</p>
                        </c:forEach>
                    </div>
                </c:if>
            </fieldset>
        </div>
    </div>
</div>
<div class='notifications top-right'></div>
<jsp:include page="scripts.jsp"/>
<script>
    $(document).ready(function () {
        $('.top-right').notify({
            message: {text: 'Если вы видите эту страницу, то произошла ошибка'},
            fadeOut: {enabled: true, delay: 1500},
            closable: false,
            type: 'info'
        }).show();

        var context = {val: '${context}'};
        setContext(context);
    })
</script>
</body>
</html>