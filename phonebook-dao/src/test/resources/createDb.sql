CREATE TABLE employee_tbl(
id INT NOT NULL AUTO_INCREMENT,
first_name VARCHAR(35) NULL,
middle_name VARCHAR(35) NULL,
last_name VARCHAR(35) NULL,
email VARCHAR(40) NULL,
icq VARCHAR(30) NULL,
skype VARCHAR(30) NULL,
birthdate DATE NULL,
extra_info TEXT NULL,
department_id INT NULL,
boss_id INT NULL,
home_address_id INT NULL,
work_address_id INT NULL,
PRIMARY KEY(id),
FOREIGN KEY(boss_id) REFERENCES employee_tbl(id)
);

CREATE TABLE work_phone_tbl(
id INT NOT NULL AUTO_INCREMENT,
phone VARCHAR(25) NOT NULL,
PRIMARY KEY(id)
);
  
CREATE TABLE employee_to_work_phone(
employee_id INT NOT NULL,
work_phone_id INT NOT NULL,
PRIMARY KEY(employee_id,work_phone_id),
FOREIGN KEY(employee_id) REFERENCES employee_tbl(id),
FOREIGN KEY(work_phone_id) REFERENCES work_phone_tbl(id)
);

CREATE TABLE personal_phone_tbl(
id INT NOT NULL AUTO_INCREMENT,
phone VARCHAR(25) NOT NULL,
employee_id INT,
PRIMARY KEY(id),
FOREIGN KEY (employee_id) REFERENCES employee_tbl(id)
);

CREATE TABLE home_address_tbl(
id INT NOT NULL AUTO_INCREMENT,
country VARCHAR(60),
city VARCHAR(40),
street VARCHAR(100),
house VARCHAR(20),
room VARCHAR(20),
postal_code INT,
extra TEXT NULL,
PRIMARY KEY(id)
);

CREATE TABLE work_address_tbl(
id INT NOT NULL AUTO_INCREMENT,
country VARCHAR(60),
city VARCHAR(40),
street VARCHAR(100),
house VARCHAR(20),
room VARCHAR(20),
postal_code INT,
extra TEXT NULL,
PRIMARY KEY(id)
);

CREATE TABLE department_tbl(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(150) NOT NULL,
head_id INT NULL,
PRIMARY KEY(id),
FOREIGN KEY(head_id) REFERENCES employee_tbl(id)
);

CREATE TABLE department_phone_tbl(
id INT NOT NULL AUTO_INCREMENT,
phone VARCHAR(25) NOT NULL,
department_id INT,
PRIMARY KEY(id),
FOREIGN KEY (department_id) REFERENCES department_tbl(id)
);

ALTER TABLE employee_tbl ADD FOREIGN KEY (home_address_id) REFERENCES home_address_tbl(id);
ALTER TABLE employee_tbl ADD FOREIGN KEY (department_id) REFERENCES department_tbl(id);
ALTER TABLE employee_tbl ADD FOREIGN KEY (work_address_id) REFERENCES work_address_tbl(id);
SET TRACE_LEVEL_SYSTEM_OUT 3;