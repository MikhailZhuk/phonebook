DROP TABLE IF EXISTS employee_tbl;
DROP TABLE IF EXISTS employee_to_work_phone;
DROP TABLE IF EXISTS personal_phone_tbl;
DROP TABLE IF EXISTS home_address_tbl;
DROP TABLE IF EXISTS work_address_tbl;
DROP TABLE IF EXISTS work_phone_tbl;
DROP TABLE IF EXISTS department_tbl;
DROP TABLE IF EXISTS department_phone_tbl;