package com.getjavajob.web06.zhukm;

import org.h2.tools.RunScript;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@DirtiesContext
@ContextConfiguration({"classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public class DepartmentDaoTest extends AbstractDaoTest {
    private Department expected;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private DataSource dataSource;

    @Before
    public void initDb() throws SQLException {
        Connection connection = dataSource.getConnection();
        RunScript.execute(connection, new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("drop.sql")));
        RunScript.execute(connection, new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("createDb.sql")));
        connection.close();
        Employee employee=getEmployee();
        employee=employeeDao.create(employee);
        expected = getDepartment();
        expected.setHead(employee);
    }

    private Department getDepartment() {
        Department department=new Department();
        department.setName("DepartmentName");
        DepartmentPhone depPhone1=new DepartmentPhone();
        depPhone1.setNumber("1");
        DepartmentPhone depPhone2=new DepartmentPhone();
        depPhone2.setNumber("2");
        DepartmentPhone depPhone3=new DepartmentPhone();
        depPhone3.setNumber("3");
        department.addPhone(depPhone1);
        department.addPhone(depPhone2);
        department.addPhone(depPhone3);
        return department;
    }

    private Employee getEmployee() {
        Employee employee;
        employee = new Employee();
        employee.setFirstName("FirstName");
        employee.setEmail("e@mail.com");
        employee.setIcq("icq");
        Calendar calendar = new GregorianCalendar(1988, 5, 29);
        Date date = new Date(calendar.getTimeInMillis());
        employee.setBirthdate(date);
        employee.setExtraInfo("extra");
        PersonalPhone personalPhone = new PersonalPhone();
        personalPhone.setNumber("111");
        PersonalPhone personalPhone2 = new PersonalPhone();
        personalPhone2.setNumber("222");
        PersonalPhone personalPhone3 = new PersonalPhone();
        personalPhone3.setNumber("333");
        employee.addPersonalPhone(personalPhone);
        employee.addPersonalPhone(personalPhone2);
        employee.addPersonalPhone(personalPhone3);
        WorkPhone workPhone = new WorkPhone();
        workPhone.setNumber("wPhone1");
        WorkPhone workPhone2 = new WorkPhone();
        workPhone2.setNumber("wPhone3");
        WorkPhone workPhone3 = new WorkPhone();
        workPhone3.setNumber("wPhone3");
        employee.addWorkPhone(workPhone);
        employee.addWorkPhone(workPhone2);
        employee.addWorkPhone(workPhone3);
        HomeAddress homeAddress = new HomeAddress();
        homeAddress.setCountry("home_country");
        homeAddress.setCity("homeCity");
        employee.setHomeAddress(homeAddress);
        WorkAddress workAddress = new WorkAddress();
        workAddress.setCountry("work_country");
        workAddress.setCity("work_city");
        employee.setWorkAddress(workAddress);
        PersonalPhone phone = new PersonalPhone();
        phone.setNumber("head_phone");
        return employee;
    }

    @Override
    public void testInsert() {
        expected = departmentDao.create(expected);
        Department actual = departmentDao.read(expected.getId());
        Assert.assertEquals(expected,actual);
    }

    @Ignore
    @Override
    public void testRead() {
        expected = departmentDao.create(expected);
        Department actual = departmentDao.read(expected.getId());
        Assert.assertEquals(expected,actual);
    }

    @Override
    public void testUpdate() {
        expected = departmentDao.create(expected);
        expected.setName("updDepName");
        expected = departmentDao.update(expected);
        Department actual = departmentDao.read(expected.getId());
        Assert.assertEquals(expected,actual);
    }

    @Override
    public void testDelete() {
        expected = departmentDao.create(expected);
        departmentDao.delete(expected);
        Assert.assertNull(departmentDao.read(expected.getId()));
    }

    @Override
    public void testGetAll() {
        Department department1 = getDepartment();
        Department department2 = getDepartment();
        Department department3 = getDepartment();
        department1=departmentDao.create(department1);
        department2=departmentDao.create(department2);
        department3=departmentDao.create(department3);
        List<Department> departments = departmentDao.getAll();
        Assert.assertEquals(department1,departments.get(0));
        Assert.assertEquals(department2,departments.get(1));
        Assert.assertEquals(department3,departments.get(2));
    }
}