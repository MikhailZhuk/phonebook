package com.getjavajob.web06.zhukm;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;

public abstract class AbstractDaoTest {

    @Test
    public abstract void testInsert();

    @Test
    public abstract void testRead();

    @Test
    public abstract void testUpdate();

    @Test
    public abstract void testDelete();

    @Test
    public abstract void testGetAll();
}