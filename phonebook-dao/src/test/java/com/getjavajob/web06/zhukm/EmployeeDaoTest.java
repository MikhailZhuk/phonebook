package com.getjavajob.web06.zhukm;

import org.h2.tools.RunScript;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@DirtiesContext
@ContextConfiguration({"classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public class EmployeeDaoTest extends AbstractDaoTest {
    private Employee expected;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private DataSource dataSource;

    @Before
    public void initDb() throws SQLException {
        Connection connection = dataSource.getConnection();
        RunScript.execute(connection, new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("drop.sql")));
        RunScript.execute(connection, new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("createDb.sql")));
        connection.close();
        expected = getEmployee();
    }

    private Employee getEmployee() {
        Employee employee;
        employee = new Employee();
        employee.setFirstName("FirstName");
        employee.setEmail("e@mail.com");
        employee.setIcq("icq");
        Calendar calendar = new GregorianCalendar(1988, 5, 29);
        Date date = new Date(calendar.getTimeInMillis());
        employee.setBirthdate(date);
        employee.setExtraInfo("extra");
        PersonalPhone personalPhone = new PersonalPhone();
        personalPhone.setNumber("111");
        PersonalPhone personalPhone2 = new PersonalPhone();
        personalPhone2.setNumber("222");
        PersonalPhone personalPhone3 = new PersonalPhone();
        personalPhone3.setNumber("333");
        employee.addPersonalPhone(personalPhone);
        employee.addPersonalPhone(personalPhone2);
        employee.addPersonalPhone(personalPhone3);
        WorkPhone workPhone = new WorkPhone();
        workPhone.setNumber("wPhone1");
        WorkPhone workPhone2 = new WorkPhone();
        workPhone2.setNumber("wPhone3");
        WorkPhone workPhone3 = new WorkPhone();
        workPhone3.setNumber("wPhone3");
        employee.addWorkPhone(workPhone);
        employee.addWorkPhone(workPhone2);
        employee.addWorkPhone(workPhone3);
        HomeAddress homeAddress = new HomeAddress();
        homeAddress.setCountry("home_country");
        homeAddress.setCity("homeCity");
        employee.setHomeAddress(homeAddress);
        WorkAddress workAddress = new WorkAddress();
        workAddress.setCountry("work_country");
        workAddress.setCity("work_city");
        employee.setWorkAddress(workAddress);
        Department department = new Department();
        department.setName("depName");
        DepartmentPhone depPhone = new DepartmentPhone();
        depPhone.setNumber("depPhone_1");
        DepartmentPhone depPhone2 = new DepartmentPhone();
        depPhone2.setNumber("depPhone_2");
        department.addPhone(depPhone);
        department.addPhone(depPhone2);
        PersonalPhone phone = new PersonalPhone();
        phone.setNumber("head_phone");
        department = departmentDao.create(department);
        employee.setDepartment(department);
        return employee;
    }

    @Override
    public void testInsert() {
        expected = employeeDao.create(expected);
        Employee actual = employeeDao.read(expected.getId());
        Assert.assertEquals(expected, actual);
    }

    @Ignore
    @Override
    public void testRead() {
    }

    @Override
    public void testUpdate() {
        expected = employeeDao.create(expected);
        expected.setFirstName("updFirstName");
        expected.setMiddleName("updMidName");
        expected = employeeDao.update(expected);
        Employee actual = employeeDao.read(expected.getId());
        Assert.assertEquals(expected, actual);
    }

    @Override
    public void testDelete() {
        expected = employeeDao.create(expected);
        employeeDao.delete(expected);
        Assert.assertNull(employeeDao.read(expected.getId()));
    }

    @Override
    public void testGetAll() {
        Employee employee1 = getEmployee();
        Employee employee2 = getEmployee();
        Employee employee3 = getEmployee();
        employee3.setFirstName("firstName3");
        employee1 = employeeDao.create(employee1);
        employee2 = employeeDao.create(employee2);
        employee3 = employeeDao.create(employee3);
        List<Employee> employees = employeeDao.getAll();
        Assert.assertEquals(employee1, employees.get(0));
        Assert.assertEquals(employee2, employees.get(1));
        Assert.assertEquals(employee3, employees.get(2));
    }

    @Test
    public void testGetByFilterNames() {
        Employee employeeA = new Employee();
        employeeA.setFirstName("A");
        employeeDao.create(employeeA);
        Employee employeeAB = new Employee();
        employeeAB.setFirstName("A");
        employeeAB.setMiddleName("B");
        employeeDao.create(employeeAB);
        Employee employeeABC = new Employee();
        employeeABC.setFirstName("A");
        employeeABC.setMiddleName("B");
        employeeABC.setLastName("C");
        employeeDao.create(employeeABC);
        List<Employee> employees = employeeDao.getByFilterNames("A");
        Assert.assertEquals(employeeA, employees.get(0));
        Assert.assertEquals(employeeAB, employees.get(1));
        Assert.assertEquals(employeeABC, employees.get(2));
        employees = employeeDao.getByFilterNames("AB");
        Assert.assertEquals(employeeAB, employees.get(0));
        Assert.assertEquals(employeeABC, employees.get(1));
        employees = employeeDao.getByFilterNames("ABC");
        Assert.assertEquals(employeeABC, employees.get(0));
        employees = employeeDao.getByFilterNames("B");
        Assert.assertEquals(employeeAB, employees.get(0));
        Assert.assertEquals(employeeABC, employees.get(1));
        employees = employeeDao.getByFilterNames("C");
        Assert.assertEquals(employeeABC, employees.get(0));
        employees = employeeDao.getByFilterNames("AC");
        Assert.assertTrue(employees.isEmpty());
        employees = employeeDao.getByFilterNames("a");
        Assert.assertEquals(employeeA, employees.get(0));
        Assert.assertEquals(employeeAB, employees.get(1));
        Assert.assertEquals(employeeABC, employees.get(2));
        employees = employeeDao.getByFilterNames("ab");
        Assert.assertEquals(employeeAB, employees.get(0));
        Assert.assertEquals(employeeABC, employees.get(1));
        employees = employeeDao.getByFilterNames("abc");
        Assert.assertEquals(employeeABC, employees.get(0));
        employees = employeeDao.getByFilterNames("b");
        Assert.assertEquals(employeeAB, employees.get(0));
        Assert.assertEquals(employeeABC, employees.get(1));
        employees = employeeDao.getByFilterNames("c");
        Assert.assertEquals(employeeABC, employees.get(0));
        employees = employeeDao.getByFilterNames("ac");
        Assert.assertTrue(employees.isEmpty());
    }

    @Test
    public void testGetByDepartment() {
        Department department = new Department();
        department.setName("department");
        department = departmentDao.create(department);
        Employee employee1 = getEmployee();
        employee1.setDepartment(department);
        employee1 = employeeDao.create(employee1);
        Employee employee2 = getEmployee();
        employee2.setDepartment(department);
        employee2 = employeeDao.create(employee2);
        List<Employee> employeesByDepartment = employeeDao.getByDepartment(department);
        Assert.assertEquals(employee1, employeesByDepartment.get(0));
        Assert.assertEquals(employee2, employeesByDepartment.get(1));
    }
}