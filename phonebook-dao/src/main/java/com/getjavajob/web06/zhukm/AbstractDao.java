package com.getjavajob.web06.zhukm;

import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public abstract class AbstractDao<T extends BaseEntity> implements CRUD<T> {

    @PersistenceContext(name = "phonebook-persistence")
    protected EntityManager em;

    private final Class<T> genericType;

    public AbstractDao() {
        this.genericType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), AbstractDao.class);
    }

    @Override
    public T create(T entity) {
        return em.merge(entity);
    }

    @Override
    public T read(int id) {
        return em.find(genericType, id);
    }

    @Override
    public void delete(T entity) {
        em.remove(em.contains(entity) ? entity : em.merge(entity));
    }

    @Override
    public T update(T entity) {
        return em.merge(entity);
    }

    public List<T> getAll() {
        Query query = em.createQuery("SELECT e FROM " + genericType.getSimpleName() + " e");
        return (List<T>) query.getResultList();
    }
}