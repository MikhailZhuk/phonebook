package com.getjavajob.web06.zhukm;

import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDao extends AbstractDao<Employee> {

    public List<Employee> getByDepartment(Department department) {
        Query query = em.createNamedQuery("Employee.byDepartment");
        query.setParameter("departmentId", department.getId());
        return (List<Employee>) query.getResultList();
    }

    public List<Employee> getByFilterNames(String filter) {
        Query query = em.createNamedQuery("Employee.filter.names");
        query.setParameter("filter", filter);
        List<Object[]> list = query.getResultList();
        List<Employee> employees = new ArrayList<>();
        for (Object[] object : list) {
            Employee employee = new Employee();
            employee.setFirstName((String) object[0]);
            employee.setMiddleName((String) object[1]);
            employee.setLastName((String) object[2]);
            employee.setId((int) object[3]);
            employees.add(employee);
        }
        return employees;
    }
}