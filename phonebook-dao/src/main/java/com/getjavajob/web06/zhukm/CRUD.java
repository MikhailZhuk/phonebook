package com.getjavajob.web06.zhukm;

import org.springframework.stereotype.Repository;

@Repository
public interface CRUD<T extends BaseEntity> {

    T create(T entity);

    T read(int id);

    T update(T entity);

    void delete(T entity);
}