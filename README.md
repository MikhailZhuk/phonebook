﻿# Phonebook

** Functionality: **

+ create/edit/delete employee
+ create/edit/delete department
+ add boss to employee
+ add head to department
+ add/delete phone input fields dynamically
+ data input validation without page reload
+ JS helpers, notifications
+ upload, instantly show avatar
+ resize avatar
+ "keep signed in" function
+ ajax sign in validation
+ ajax search
+ JS/JQuery animation effects  

** Tools: **  
JDK 7, Spring 4, JPA 2 / Hibernate 4, jQuery 1, Twitter Bootstrap 3, Bootstrap Notify, JUnit 4, JSP/JSTL, Jackson 2,Maven 3, Git / Bitbucket, Tomcat 7, MySql, H2Database, Log4j, IntelliJIDEA 15.  

--  
**Жук Михаил**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)

![1.jpg](https://bitbucket.org/repo/xq5BMz/images/60708006-1.jpg)
![3.jpg](https://bitbucket.org/repo/xq5BMz/images/2763779613-3.jpg)
![4.jpg](https://bitbucket.org/repo/xq5BMz/images/1350997527-4.jpg)
![2.jpg](https://bitbucket.org/repo/xq5BMz/images/516447955-2.jpg)