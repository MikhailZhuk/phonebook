package com.getjavajob.web06.zhukm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ServiceLayer {

    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private EmployeeDao employeeDao;

    public Employee getEmployee(int id) {
        return employeeDao.read(id);
    }

    public List<Employee> getEmployees() {
        return employeeDao.getAll();
    }

    public List<Employee> getByDepartment(Department department) {
        return employeeDao.getByDepartment(department);
    }

    @Transactional
    public Employee createEmployee(Employee employee) {
        return employeeDao.create(employee);
    }

    @Transactional
    public Employee updateEmployee(Employee employee) {
        return employeeDao.update(employee);
    }

    @Transactional
    public void deleteEmployee(Employee employee) {
        employeeDao.delete(employee);
    }

    public List<Employee> searchEmployee(String filter) {
        return employeeDao.getByFilter(filter);
    }

    public List<Employee> searchEmployeeNames(String filter) {
        return employeeDao.getByFilterNames(filter);
    }

    public Department getDepartment(int id) {
        return departmentDao.read(id);
    }

    public List<Department> getAllDepartments() {
        return departmentDao.getAll();
    }

    @Transactional
    public Department createDepartment(Department department) {
        return departmentDao.create(department);
    }

    @Transactional
    public Department updateDepartment(Department department) {
        return departmentDao.update(department);
    }

    @Transactional
    public void deleteDepartment(Department department) {
        departmentDao.delete(department);
    }
}